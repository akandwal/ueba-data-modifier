from packages import config
from packages import date_modifier as dm
from packages import time_modifier as tm
from packages import user_modifier as um
from packages import common

if __name__ == "__main__":
    #drop collection if it exists
    db = config.get_database()
    db[config.config_reader('Storage', 'NEW_COLLECTION')].drop()
    
    col = config.get_collection()
    
    if config.config_reader('User', 'IS_USER_SPECIFIC'):
        for doc in col.find():
            new_user = um.user_define(doc, 'User')
            db[config.config_reader('Storage', 'NEW_COLLECTION')].insert_one(new_user)
    else:
        for doc in col.find():        
            new_Date = dm.date_define(doc['startInstant'], 'Days')
            new_Time = tm.time_define(doc['startInstant'], 'Time')      
            db[config.config_reader('Storage', 'NEW_COLLECTION')].insert_one(common.create_doc(doc, new_Date, new_Time))
    print("Code generated")