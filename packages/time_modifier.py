# import os, sys
# sys.path.append('C:/Gitlab/ueba-data-modifier')

from packages import config
import datetime as dt
from random import randint,randrange
from datetime import datetime as dt
import random

    
    
#fetches time from data
def time_define(self, datetime, *argv):
    time = self.on_time(dt.time(datetime), *argv)
    return time 
    

#check if the time is valid
def on_time(self, timestamp, *argv):
    time = timestamp
    sh = int(config.config_reader(*(argv+('STARTHOUR',))))
    eh = int(config.config_reader(*(argv+('ENDHOUR',))))
    
    if sh > int(time.hour) or int(time.hour) > eh:
        time = self.random_time(*argv)
    return time

#This function will return a random time between two datetime 
def random_time(*argv):    
    hh = str(random.randint(int(config.config_reader(*(argv+('STARTHOUR',)))),int(config.config_reader(*(argv+('ENDHOUR',))))))
    mm = str(random.randint(int(config.config_reader(*(argv+('STARTMIN',)))),int(config.config_reader(*(argv+('ENDMIN',))))))
    ss = str(random.randint(int(config.config_reader(*(argv+('STARTSEC',)))),int(config.config_reader(*(argv+('ENDSEC',))))))
    return dt.strptime(f'{hh}:{mm}:{ss}', "%H:%M:%S")