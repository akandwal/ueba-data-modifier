import os, sys
sys.path.append('C:/Gitlab/ueba-data-modifier')

from packages import config
from packages import common
from packages import time_modifier as tm
from packages import date_modifier as dm
import inflect
#######################################################################
#   -> Pick groups from properties.
#   -> Check for time and date accordingly
#   -> call date and time functions
#   -> call common function
#######################################################################

p = inflect.engine()

# *argv -> User
def user_define(doc, *argv):
    user = user_grouping(doc, *argv)
    return user

def user_generator(doc, *group):
    #based on the grouping of the user fetch the date and time
    new_date = dm.date_define(doc['startInstant'], *(group + ('Days',)))
    new_time = tm.time_define(doc['startInstant'], *(group + ('Time',)))
    new_user = common.create_doc(doc, new_date, new_time)
    return new_user

def user_grouping(doc, *argv):
    for i in config.config_reader(*(argv + ('Group1', 'Define', 'CATEGORY',))):
        if i.casefold() in doc['userId'].casefold():
            for j in group_range(*(argv + ('Group1', 'Define', 'RANGE',))):
                if j in doc['userId']:
                    return user_generator(doc, *(argv+('Group1',)))
    
    for i in config.config_reader(*(argv + ('Group2', 'Define', 'CATEGORY',))):
            if i.casefold() in doc['userId'].casefold():
                for j in group_range(*(argv + ('Group2', 'Define', 'RANGE',))):
                    if j in doc['userId']:
                        return user_generator(doc, *(argv+('Group2',)))

    return user_generator(doc, *(argv+('Group1',)))

def group_range(*argv):
    a = list()
    for i in range(int(config.config_reader(*argv)[0]), int(config.config_reader(*argv)[1])+1):
        a.append(p.number_to_words(i))
    return a

