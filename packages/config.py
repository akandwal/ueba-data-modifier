from configobj import ConfigObj
import pymongo as mongo

#connecting to config property file
config = ConfigObj('resources/application.properties')

#Reads and returns data from property file
def config_reader(*argv):
    value = config.get(argv[0]) 
    for i in argv[1:]:
        value = value.get(i)
    return value

#Connecting to mongoDB -> database -> collection
def connect_mongo():
    conn = mongo.MongoClient(config_reader('Connection', 'URL'))
    return conn

def get_database():
    conn = connect_mongo()
    db = conn[config_reader('Storage', 'DATABASE')]
    return db

def get_collection():
    db = get_database()
    col = db[config_reader('Storage', 'COLLECTION')]
    return col