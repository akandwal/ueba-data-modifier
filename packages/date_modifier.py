import datetime as dt
from random import randint
from packages import config
from packages import common

#Values that randomly picked from array of data
def random_pick(ar):
    return ar[randint(0,(len(ar)-1))]

#Finds day
def day_checker(date):
    return date.strftime('%A')

#Creates dictionary as Day:Num - to use for day difference
def index_day(day):
    week_dict = common.index_maker(config.config_reader('Days', 'WEEK'), config.config_reader('Days', 'INDEX'))
    return week_dict[day]

#Converts to On day
def on_day(date, *argv):
    day = day_checker(date)
    if day in config.config_reader(*(argv+('OFF_DAYS',))):
        day = random_pick(config.config_reader(*(argv+('ON_DAYS',))))
    return day

#finds the first day of the month
def first_of_month(date):
    first = dt.date(date.year, date.month, 1)
    return first

#Randomly pick a date for the converted day
def date_finder(day, first):
    date = first
    d1 = int(index_day(day))
    d2 = int(index_day(day_checker(first)))
    if d1 >= d2:
        date += dt.timedelta(d1-d2)
    else:
        date += dt.timedelta((7-(d2-d1)))
    return  date   
    
#Accumulates all specific day's dates 
def date_accumulator(date, *argv):
    dateList = list()
    d1 = date_finder(on_day(date, *argv), first_of_month(date))
    dateList.append(d1)
    for i in range(6):
        d1 += dt.timedelta(days=7)
        if(d1.month == date.month):
            dateList.append(d1)
    return dateList

#Assigning new date
def date_define(datetime, *argv):
    newDate = random_pick(date_accumulator(dt.datetime.date(datetime), *argv))
    return dt.datetime(newDate.year, newDate.month, newDate.day)
