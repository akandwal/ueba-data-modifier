from datetime import datetime as dt

def get_datetime(date, time):
    return dt(date.year, date.month, date.day, time.hour, time.minute, time.second)

def create_doc(doc, date, time):
    new_doc = doc.copy()
    new_doc['startInstant'] = get_datetime(date, time)
    return new_doc

def index_maker(key, value):
    return dict(zip(key, value))
    
    