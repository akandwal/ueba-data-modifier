# UEBA-data-modifier

## Table of Contents
1. [Requirements](#requirements)
2. [Libraries used](#libraries)
3. [Overview of code](#ovc)
4. [Overview of functions](#ovf)

### Requirements

This project requires data to be generated using the offline data generator and ingest them to the EPLH(Endpoint LogHybrid) to upload it to UEBA and mongo. From mongo, data needs to be imported.

### Libraries used

1. __datetime__ module : 
```bash 
pip install datetime
```
2. __random__ module
```python
pip install random
```
3. __pymongo__ module
```python
pip install pymongo
```
4. __configobj__ module
```python
pip install configobj
```

### Overview of Code:

* Three separate functions are written to modify __date, time & user__ attributes of the enriched_authentication collection. 
* The functions are imported in the form of packages and read in the ___main.py___ and called upon loading the data from the mongo using __config.py__ which gives the desired output.

### Property File:
The main use of propertiy file(___application.properties & user.properties___) is to store the configuration parameters, which can be used by program during runtime.
* Here under sections of property file, we have stored __ON_DAYS__(working days) ,__OFF_DAYS__, Connection and host of __mongoDB__, and the __TIME__ properties and have used these values during program execution.


### Overview of Functions:
1. From __config.py__:
* ___connect_mongo & get_database___   : All data from ___application.properties___ are read using __config__ module and are used to connect to mongoDB, and get the corresponding database and collection.
2. From __time_modifier.py__:
* ___def on_time___ : checks whether the timestamp falls outside the range, taken from ___application.properties___
* ___def random_time___ : generates random time stamp in between the START_HOUR & END_HOUR

3. From __date_modifier.py__ :
* ___def index_day___ : Collects data from ___application.properties___ and creates a dictionary like {Sunday: 0, Monday: 1} and so on..
* __def on_day___ : Converts OFF days to ON days based on ___application.properties___
* ___def date_finder___ : Allocates a date for the converted ON days using ___timedelta___ function.
*___def date_accumulator___ : Accumulates the above dates generated to be randomly picked by ___date_define()___ 
4. From __user_modifier.py__:
